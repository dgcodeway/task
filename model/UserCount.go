package model

type UserCount struct {
	UserCount int64 `json:"user_count" bigquery:"user_count"`
}
