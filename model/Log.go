package model

type Log struct {
	Type      string `json:"type"`
	AppId     string `json:"app_id"`
	SessionId string `json:"session_id"`
	EventName string `json:"event_name"`
	EventTime int64  `json:"event_time"`
	Page      string `json:"page"`
	Country   string `json:"country"`
	Region    string `json:"region"`
	City      string `json:"city"`
	UserId    string `json:"user_id"`
}
