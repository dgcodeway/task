package model

type DailyDurationSummary struct {
	AverageDuration float64 `json:"average_duration"`
}
