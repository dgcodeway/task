FROM golang:latest
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
ENV GOOGLE_APPLICATION_CREDENTIALS ./our-shield-311009-db3a420c9a0f.json
RUN go build -o main .
EXPOSE 8080
CMD ["./main"]