# Task

Shorty, I used Gitlab as CI/CD environment and used Google Cloud's technologies you mentioned in the task. I dockerized
the app and run in the Google Cloud's Kubernetes Engine. You can find the details below.

## Accounts

#### Gmail and Google Cloud:

Username:  dgcodeway@gmail.com  
Password: dgcodeway123

#### Gitlab:

Username:  dgcodeway@gmail.com  
Password: dgcodeway123

#### Dockerhub:

Username:  dgcodeway  
Password: dgcodeway123

# Test the application

Application works on 34.77.145.222:8765 IP and port. You can send CURL request to that IP.

Send Post request `34.77.145.222:8765/api/log` with the sample message in the task in message body

Send Get request `34.77.145.222:8765/api/daily/user/active/{time}` (Time in milliseconds)

Send Get request `34.77.145.222:8765/api/daily/hour/{time}` (Time in milliseconds)

Send Get request `34.77.145.222:8765/api/users/count`

## How to build

I used Gitlab as CI/CD environment. If you make any change and push it, Gitlab builds, tests, dockerizes and deploys
automatically. To see the settings check ".gitlab-ci.yml" file.

In local, to execute the app follow the steps below:

1. Save the text below as a json file to your file system

```json
{
  "type": "service_account",
  "project_id": "our-shield-311009",
  "private_key_id": "db3a420c9a0f8c74c7bd2018e0f270d0b4ff8338",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCx0HwzSIowAi51\n2kbLkiQf+jmccSD1xEYsLBu9VfVb11hZPZ11MzOyCrnGRw7VVfraeDWmljOPt9QJ\np++5bs7BnL31yj0WrCtNAQmeXrGvxobbNssQZUoJUi1yvmjBagn7HJ6C1GCLfeux\nmwUAla9zKiJJ+jT4iReWPirmRm2pqKWiJ5GVDNfylWbr/F/D+PN/s4E6OLibcNUi\nOOMRawADBg0p+xdnPQFj//veR9ok2RnWnLrA5g4wPmrKrUBx2Z1stc6Jtxc5sxp+\nzOpWFH7ZYehmhkajwSAIen78s9jmv0tci8FZAY3iMoBbMG+7RhAyI/911u2MEWUs\n+uX5+tq3AgMBAAECggEAM3LqBOKnzTVMYSA7nLpXxoLduYTILyRP04cdrZ5aBG1e\nLFAWJ6udeRX+Y776rUKX9y+SAnXyTFK5wON9Cu/jVgT2t9AA9fbx/nFe6Zpnmj78\nlSAZCFDuZ50F9m3enJwEcEp7GMq/do0ELdr3SxCEd/OIgVQqkIEGOJwbSI63Oz/L\nfL0xYJkOA0zsq9rQST61rfVDHygDWKZ29OI1uEATuvaJxNqP082unjx1xIC9/TkS\nGKzR0cneoqtMsDyiqRERP30NGVv+k+A7gonghN+v9nzq56yNiTCadB8At4JbGdcz\nFS76PuK9mo8Wwl+PbcVDE8x96lVA0FG1nl3WeF80aQKBgQDmWeueMWZzQ7GZFun6\nmQCzuuCfKavgq35IR1the6BacwtvLtladgDY71k/E2gD4qLjY0xE8FDFTYEr9BJ5\njLKMsllG/em9jO5zqUpu+HiOuqUUgfkas6SErcPoWkukQfKqPCOx08E4c6IJNEg4\ntMK2ojfA4o+wLQf+ntiJb0hVLwKBgQDFnQVEHgeZ1f6RnfhwW9x5It/ldE8c+4bL\nv48AGOViZ4xxzXgeEn/q0HPXV2R1IYiNu8dIj2oclzE40ssKwHZeaHpYAYKgdzHW\nT58n48XKi9lQmdBvJx3xAsr9VocIO1So5cmIsc8wBnJ6FrxzFX03TCulVd6oTm5W\nL16HJWAA+QKBgQDPoL2AGYb+3kV2d/DusHqxhgBxdl42ZF2vSKJVe9lNreTgkpnX\nsAWThCotcJCQ7/difl6AxRwIu5NyyszcQrWtnBRcqQHGmQFVv4IYBF6mSBYEJiMi\nrkncTcQH61vC8q6IQyzpXLUM7S1gnn96nXPkqtMWSSC669wI5aTIPEY2gQKBgQC7\nnJO/U/NLbgs+HIgdxFjQ+PcwVPg3P+V90uPk573/aue/s57yjgW73SPdsi7BtZB7\nX2eXz/CiLDijBh2Wm5rFjtN4chaeWguKuBUWPf0uiw9Kon1syep3QnuNgXS5r/p0\nT5TCnm1h3v0YRDWt3R0APuTBF4FIsQ237UkFbcpzoQKBgFY4U77dpzofHp1FIETb\nuaXRu8/BjA4L+DCDNovtjBTMiCAqDxd1WgpunZN1H5X6v/5I86DmaI3FyA8H/yqV\nL9mSMzOdcSU8miyqR5HTTXJHax8smVeR7EOFDqg+RXaaeEif08KcNJHgkJ8Y/lM4\nF0urpUu+2wrEGtuXVIfioOPA\n-----END PRIVATE KEY-----\n",
  "client_email": "dgcodeway@our-shield-311009.iam.gserviceaccount.com",
  "client_id": "102124198346033632917",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/dgcodeway%40our-shield-311009.iam.gserviceaccount.com"
}
````

2. Set GOOGLE_APPLICATION_CREDENTIALS environment variable and as value write the json file's path
3. Go to task folder
4. Execute `go get`
5. Execute `go build -o loggingApp.exe` or in linux `go build -o loggingApp`
6. Execute `loggingApp.exe` or n Linux `./loggingApp`

## How to maintain multiple environments

I created a production environment. Production environment is in Google Cloud kubernetes engine. There is a load
balancer which exposes pods to outside. Pods with the docker images are created with deployment file in k8s folder.

If there were a need to set up another environment, I would create another cluster in the Google Cloud kubernetes
engine. Then I would create necessary kubernetes yml files. Finally, I would add another step to Gitlab configuration
file and make configuration just similar to the deploy configuration.

## How to maintain this in a  micro-service environment?

I believe that this application can be a part of micro-service architecture with some changes. If the microservices uses
sagas, I need to add extra service calls to the app. If the number of the request per pod increases, I would increase
the number pods in the deployment yml or develop an autoscaling mechanism for the kubernetes cluster.