package services

import "gitlab.com/dgcodeway/task/model"

type LogServices interface {
	SaveLog(log *model.Log) error
	GetDailyActiveUsers(unixTime int64) ([]model.UserInfo, error)
	GetDailyAverageDuration(unixTime int64) (*model.DailyDurationSummary, error)
	GetTotalUserCount() (*model.UserCount, error)
}
