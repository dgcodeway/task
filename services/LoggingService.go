package services

import (
	"gitlab.com/dgcodeway/task/model"
	"gitlab.com/dgcodeway/task/repository"
	"gitlab.com/dgcodeway/task/utils"
)

var (
	LogService = NewLogService()
)

type appLogService struct {
}

func NewLogService() *appLogService {
	return &appLogService{}
}

func (l appLogService) SaveLog(log *model.Log) error {
	return repository.Repo.AddLog(log)
}

func (l appLogService) GetDailyActiveUsers(unixTime int64) ([]model.UserInfo, error) {
	beginning := utils.GetBeginningOfTheDay(unixTime)
	end := utils.GetEndOfTheDay(unixTime)
	return repository.Repo.ListDailyActiveUsers(beginning, end)
}

func (l appLogService) GetDailyAverageDuration(unixTime int64) (*model.DailyDurationSummary, error) {
	beginning := utils.GetBeginningOfTheDay(unixTime)
	end := utils.GetEndOfTheDay(unixTime)
	return repository.Repo.ComputeDailyAverageDuration(beginning, end)
}

func (l appLogService) GetTotalUserCount() (*model.UserCount, error) {
	return repository.Repo.ListTotalUserCount()
}
