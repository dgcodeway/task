package repository

import (
	"cloud.google.com/go/bigquery"
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/dgcodeway/task/model"
	"google.golang.org/api/iterator"
	"log"
	"math/big"
)

var (
	topic *pubsub.Topic
	Repo  LogRepository = NewBigQueryLogRepo()
)

const (
	projectName = "our-shield-311009"
	topicName   = "logs-topic"
)

type bigQueryLogRepo struct {
	TopicName   string
	ProjectName string
}

func NewBigQueryLogRepo() *bigQueryLogRepo {
	bigQueryRepo := &bigQueryLogRepo{topicName, projectName}
	bigQueryRepo.initTopic()
	return bigQueryRepo
}

func (logRepository bigQueryLogRepo) initTopic() {
	topic = getPubSubClient(logRepository.ProjectName).Topic(logRepository.TopicName)
}

func (logRepository bigQueryLogRepo) AddLog(log *model.Log) error {
	ctx := context.Background()

	out, err := json.Marshal(log)
	if err != nil {
		return errors.New("COULD NOT CONVERT TO JSON")
	}

	if _, err := topic.Publish(ctx, &pubsub.Message{Data: out}).Get(ctx); err != nil {
		return errors.New("COULD NOT PUBLISH")
	}

	return nil
}

func (logRepository bigQueryLogRepo) ListDailyActiveUsers(dayStart, dayEnd int64) ([]model.UserInfo, error) {
	client := getBigQueryClient()
	defer client.Close()
	ctx := context.Background()
	queryString := fmt.Sprintf(`SELECT  DISTINCT user_id  FROM`+" `our-shield-311009.codewaylogs.LOGS`"+`WHERE event_time between %d and %d `, dayStart, dayEnd)
	fmt.Println(queryString)
	rows, err := query(ctx, client, queryString)
	if err != nil {
		log.Println(err)
	}

	var users = []model.UserInfo{}

	for {

		var userInfo model.UserInfo
		err := rows.Next(&userInfo)

		if err == iterator.Done {
			return users, nil
		}

		if err != nil {
			return nil, err
		}

		users = append(users, userInfo)
	}

	return users, nil
}

func (logRepository bigQueryLogRepo) ComputeDailyAverageDuration(dayStart, dayEnd int64) (*model.DailyDurationSummary, error) {
	client := getBigQueryClient()
	defer client.Close()
	ctx := context.Background()
	queryString := fmt.Sprintf(`
	select SUM(duration)/COUNT(*) as average_duration from (
		select session_id, max(log.event_time)- min(log.event_time) as duration  from `+" `our-shield-311009.codewaylogs.LOGS`"+` log
			where log.session_id in (
				SELECT session_id from (
				SELECT user_id,session_id
			FROM`+" `our-shield-311009.codewaylogs.LOGS`"+
		`WHERE event_time between %d and %d
			GROUP BY user_id,session_id))
			group by (session_id)
			)
		`, dayStart, dayEnd)

	rows, err := query(ctx, client, queryString)
	if err != nil {
		log.Println(err)
	}

	type result struct {
		AverageDuration *big.Rat `bigquery:"average_duration"`
	}
	value := &result{}

	err2 := rows.Next(value)
	if err2 != nil {
		return nil, errors.New("QUERY ERROR")
	}

	var summary model.DailyDurationSummary

	if value.AverageDuration != nil {
		f, _ := value.AverageDuration.Float64()
		summary.AverageDuration = f
	}
	return &summary, err2
}

func (logRepository bigQueryLogRepo) ListTotalUserCount() (*model.UserCount, error) {
	client := getBigQueryClient()
	defer client.Close()
	ctx := context.Background()

	queryString := `SELECT  COUNT(DISTINCT user_id ) as user_count FROM  ` + "`our-shield-311009.codewaylogs.LOGS`"
	rows, err := query(ctx, client, queryString)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var userCount model.UserCount
	err2 := rows.Next(&userCount)
	if err2 != nil {
		return nil, errors.New("QUERY ERROR")

	}

	return &userCount, err2
}

func getPubSubClient(projectId string) *pubsub.Client {
	ctx := context.Background()

	client, err := pubsub.NewClient(ctx, projectId)
	if err != nil {
		log.Fatal(err)
	}

	return client
}

func getBigQueryClient() *bigquery.Client {
	ctx := context.Background()

	client, err := bigquery.NewClient(ctx, projectName)
	if err != nil {
		log.Fatal(err)
	}
	return client
}

func query(ctx context.Context, client *bigquery.Client, stringQuery string) (*bigquery.RowIterator, error) {
	query := client.Query(stringQuery)
	return query.Read(ctx)
}
