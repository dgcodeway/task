package repository

import "gitlab.com/dgcodeway/task/model"

type LogRepository interface {
	AddLog(log *model.Log) error
	ListDailyActiveUsers(dayStart, dayEnd int64) ([]model.UserInfo, error)
	ComputeDailyAverageDuration(dayStart, dayEnd int64) (*model.DailyDurationSummary, error)
	ListTotalUserCount() (*model.UserCount, error)
}
