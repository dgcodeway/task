package main

func main() {
	app := LoggingApp{Port: "8080"}
	app.initialize()
	app.startServer()
}
