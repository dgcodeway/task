package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/dgcodeway/task/restcontrollers"
	"log"
	"net/http"
)

type LoggingApp struct {
	Port string
}

func (app *LoggingApp) initialize() {
	log.Println("App started")
}

func (app *LoggingApp) startServer() {
	router := mux.NewRouter().PathPrefix("/api").Subrouter()

	router.HandleFunc("/log", restcontrollers.LogHandler).Methods(http.MethodPost)
	router.HandleFunc("/daily/user/active/{time}", restcontrollers.DailyActiveUserHandler).Methods(http.MethodGet)
	router.HandleFunc("/daily/hour/{time}", restcontrollers.DailyAverageDurationHandler).Methods(http.MethodGet)
	router.HandleFunc("/users/count", restcontrollers.TotalUserHandler).Methods(http.MethodGet)
	router.Use(mux.CORSMethodMiddleware(router))
	router.Use(loggingMiddleware)
	router.Use(responseMiddleware)

	srv := &http.Server{
		Handler: router,
		Addr:    ":" + app.Port,
	}

	log.Fatal(srv.ListenAndServe())
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func responseMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
		w.Header().Add("Content-Type", "application/json")
	})
}
