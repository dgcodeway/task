package utils

import (
	"strings"
	"time"
)

func GetBeginningOfTheDay(unixTime int64) int64 {
	unix := time.Unix(unixTime, 0)
	date := time.Date(unix.Year(), unix.Month(), unix.Day(), 0, 0, 0, 0, unix.Location())
	return date.Unix()
}

func GetEndOfTheDay(unixTime int64) int64 {
	unix := time.Unix(unixTime, 0)
	date := time.Date(unix.Year(), unix.Month(), unix.Day(), 23, 59, 59, 0, unix.Location())
	return date.Unix()
}

func StringEmpty(text string) bool {
	if len(strings.TrimSpace(text)) == 0 {
		return true
	} else {
		return false
	}
}
