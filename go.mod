module gitlab.com/dgcodeway/task

go 1.15

require (
	cloud.google.com/go/bigquery v1.17.0
	cloud.google.com/go/pubsub v1.10.2
	github.com/gorilla/mux v1.8.0
	google.golang.org/api v0.43.0
)
