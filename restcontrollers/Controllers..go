package restcontrollers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/dgcodeway/task/model"
	"gitlab.com/dgcodeway/task/services"
	"gitlab.com/dgcodeway/task/utils"
	"net/http"
	"strconv"
)

func LogHandler(w http.ResponseWriter, r *http.Request) {

	log := &model.Log{}
	err := json.NewDecoder(r.Body).Decode(log)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = services.LogService.SaveLog(log)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func DailyActiveUserHandler(w http.ResponseWriter, r *http.Request) {
	timeValue := mux.Vars(r)["time"]

	if utils.StringEmpty(timeValue) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	unixTime, parseError := strconv.ParseInt(timeValue, 10, 64)

	if parseError != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	users, err := services.LogService.GetDailyActiveUsers(unixTime)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		jsonError := json.NewEncoder(w).Encode(users)
		if jsonError != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func DailyAverageDurationHandler(w http.ResponseWriter, r *http.Request) {
	timeValue := mux.Vars(r)["time"]

	if utils.StringEmpty(timeValue) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	unixTime, parseError := strconv.ParseInt(timeValue, 10, 64)

	if parseError != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	average, err := services.LogService.GetDailyAverageDuration(unixTime)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		jsonError := json.NewEncoder(w).Encode(average)
		if jsonError != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func TotalUserHandler(w http.ResponseWriter, r *http.Request) {
	userCount, err := services.LogService.GetTotalUserCount()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		jsonError := json.NewEncoder(w).Encode(userCount)
		if jsonError != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}
